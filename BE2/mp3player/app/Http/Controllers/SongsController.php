<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Song;

class SongsController extends Controller {
    
    public function uploadSong(Song $song) {
        $song->title = request("title");
        $song->artist = request("artist");
        $song->album = request("album");
        $song->duration = request("length");

        $song->save();

        return redirect("/player");
    }


    public function displaySongs() {
        $songs = Song::all();

        return view("/player", ["songs" => $songs]);
    }

}
