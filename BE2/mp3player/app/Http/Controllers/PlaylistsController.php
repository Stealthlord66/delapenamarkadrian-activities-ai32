<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Playlist;

class PlaylistsController extends Controller {
    
    public function createPlaylist(Playlist $playlist) {
        $playlist->name = request("name");

        $playlist->save();

        return redirect("/player");
    }


    public function displayPlaylists() {
        $playlists = Playlist::all();

        return view("/player", ["playlist" => $playlists]);
    }

}
