<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Song extends Model {
    
    public $title, $artist, $album, $duration, $createdAt, $updatedAt;

    function __construct($title, $artist, $album, $duration) {
        $this->title = $title;
        $this->artist = $artist;
        $this->album = $album;
        $this->duration = $duration;

        // $this->createdAt = date("m d, Y h:i:s a");
        // $this->updatedAt = date("m d, Y h:i:s a");

    }

}
