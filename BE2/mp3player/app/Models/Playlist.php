<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model {

    public $name, $createdAt, $updatedAt;

    function __construct($name) {
        $this->name = $name;

        // $this->createdAt = date("m d, Y h:i:s a");
        // $this->updatedAt = date("m d, Y h:i:s a");
    }
    
}
