<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaylistSong extends Model {
    
    public $title, $artist, $playlistName, $createdAt, $updatedAt;

    function __construct($title, $artist, $playlistName) {
        $this->title = $title;
        $this->artist = $artist;
        $this->playlistName = $playlistName;

        // $this->createdAt = date("m d, Y h:i:s a");
        // $this->updatedAt = date("m d, Y h:i:s a");
    }

}
